import React from 'react';
import GlobalStyles from './globalStyles';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './pages/HomePage/Home';
/* import Services from './pages/Services/Services'; */
import InfoSection from './components/InfoSection/InfoSection'; 
import Palestrantes from './components/Palestrantes/Palestrantes'; 
import Products from './pages/Products/Products';
import SignUp from './pages/SignUp/SignUp';

import { Navbar, Footer } from './components';
import ScrollToTop from './components/ScrollToTop';


function App() {
  return (
    <Router>
      <GlobalStyles />
      <ScrollToTop />
      <Navbar />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/InfoSection" exact component={InfoSection} />
        <Route path="/Palestrantes" exact component={Palestrantes} />
         <Route path="/products" exact component={Products} /> 
        {/*<Route path="/products" exact component={Products} /> */}
       {/*  <Route path="/sign-up" exact component={SignUp} /> */}
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
