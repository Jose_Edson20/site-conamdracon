 export const homeObjOne = {
    lightBg: false,
    primary: true,
    imgStart:'',
    lightTopLine: true, 
    lightTextDesc: true,
    /* buttonLabel: 'Get Started', */ 
    description: 'Tecnologias: Acessibilidades e biotecnologias nas doenças raras. Objetivamos neste grupo de trabalho proporcionar espaço para reflexões e apresentação de projetos relacionados a aplicabilidade de novas tecnologias nos sistemas de inclusão. Expondo os usos de âmbito tecnológico de acordo com as mais diversas áreas de competência, direcionadas a mobilidade urbana, acessibilidades, mapeamento de dados, realidade virtual, robótica e biotecnologias.',
    headLine: 'Grupo 3', 
    lightText: true, 
    topLine: '',
    img: require('../../images/Grupo3.jpeg').default,
    alt: 'Image',
    start: ''
} 

 export const homeObjTwo = {
    lightBg: true,
    primary: false,
    /* imgStart:'start', */
    lightTopLine: false, 
    lightTextDesc: false,
    buttonLabel: 'Get Started', 
    description: '',
    headLine: 'Submeta seu Trabalho!', 
    lightText: false, 
    topLine: 'Marketing Agency', 
    /* img: require('../../images/profile-1.jpg').default, */ 
    /* alt: 'Image', */ 
    start: 'true'
} 

export const homeObjThree = {
    lightBg: false,
    primary: true,
    imgStart:'',
    lightTopLine: true, 
    lightTextDesc: true,
  /*   buttonLabel: '',  */
    topLine: 'Grupos de trabalho',
    headLine: 'Grupo 1',
    description: 'Saúde: Diretrizes e terapêutica nas doenças raras; Neste grupo de trabalho discutiremos os aspectos relacionados a terapêutica considerando a amplitude dos campos das ciências da saúde, como a fisioterapia, enfermagem, medicina, odontologia, entre outras. Compreendendo estudos e pesquisas relacionadas as doenças raras desde o diagnóstico, cuidados e tratamentos realizando reflexões relacionadas diretrizes da saúde pública.',
    lightText: true, 
    img: require('../../images/Grupo1.jpeg').default,
    alt: 'Image',
    start: ''
}


 export const homeObjFour = {
    lightBg: false,
    primary: true,
    imgStart:'start',
    lightTopLine: true, 
    lightTextDesc: true,
   /*  buttonLabel: 'Get Started', */ 
    description: 'Humanidades: Direitos, inclusão, educação e espiritualidades nas doenças raras; Realizar discussões em relação as legislações que asseguram e promovam os direitos das pessoas acometidas por doenças raras. Abrangendo assuntos relacionados a inclusão social e atividades pedagógicas, de acordo com novas perspectivas entre avanços e aplicabilidades a pessoas com doenças raras. Estendendo-se as observações holísticas e percepções de âmbito filosófico, psicológico e das religiões.',
    headLine: 'Grupo 2', 
    lightText: true, 
    topLine: '',
    img: require('../../images/Grupo2.jpeg').default,
    alt: 'Image',
    start: 'true'
} 