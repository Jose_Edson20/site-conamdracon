import React from 'react';
 import { homeObjOne,
         homeObjTwo,
         /* homeObjThree,
         homeObjFour */ } from './Data';
import { InfoSection, Pricing } from '../../components/';
import MainContent from '../../components/Main/MainContent';
import map from '../../components/map/map';
import Carousel from '../../components/Carousel/CarouselContainer';



const Home = () => {
    return (
        <>
          <div className = "App">
            <Carousel/>
         </div>
            <MainContent />
            <InfoSection {...homeObjOne}/>
             <Pricing />
              <InfoSection {...homeObjTwo}/> 
           {/*  <InfoSection {...homeObjThree}/>
            <InfoSection {...homeObjFour}/>  */}
        </>
    )
}

export default Home
