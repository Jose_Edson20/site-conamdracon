export const homeObjOne = {
    lightBg: false,
    primary: true,
    imgStart:'',
    lightTopLine: true, 
    lightTextDesc: true,
   /*  buttonLabel: 'Get Started', */ 
    address: 'Endereço: BR 230, km14 – CEP 58109-303. Cabedelo, PB',
    description: 'O Centro Universitário UNIESP é uma das maiores instituições de ensino do estado da Paraíba. Sua estrutura, que corresponde a 27.649m², está dividida em 08 blocos, onde estão as diversas salas, laboratórios e auditórios com todos os equipamentos necessários para a realização de grandes eventos, principalmente na área de educação.',
    headLine: 'Centro Universitário UNIESP',
    lightText: true, 
    topLine: 'Local do Evento',
    img: require('../../images/Uniesp21.jpeg').default,
    alt: 'Image',
    start: ''
}

export const homeObjTwo = {
    lightBg: false,
    primary: false,
    imgStart:'start',
    lightTopLine: true, 
    lightTextDesc: true,
    /* buttonLabel: '', */
    address: 'João Pessoa - PB lhes dá as boas-vindas na certeza de que faremos o que estiver ao nosso alcance para que seja um momento inesquecível e que possamos corresponder às expectativas e cumprir com tudo que foi proposto. Que Deus nos abençoe!', 
    description: 'A Comissão Organizadora se sente honrada em apresentar à Comunidade Cientifica e à Sociedade em geral a Programação Oficial do II Congresso Nacional Multidisciplinar em Doenças Raras e Anomalias Congênitas - CONAMDRACON - que acontecerá em João Pessoa no período de 03 a 05 de Novembro de 2021 no Auditório da UNIESP. Para nossa satisfação e orgulho o Evento contará com a presença de Cientistas Nacionais e Internacionais de maior destaque no âmbito das Pesquisas, Diagnóstico e Tratamento das Doenças Raras, profissionais esses que se destacam não apenas pela sapiência, mas, sobretudo, pelo comprometimento e Humanidade no tratar. Em face disso nos antecipamos em agradecimentos aos que se disponibilizaram a nos honrar não só com suas presenças, mas também com a colaboração na sugestão de temáticas, idealismo e dedicação ao planejamento, para que, ao final, pudéssemos apresentar ao Público esse nível de Programação Cientifica. Será mais um ano de fortalecimento aos Estudos e Pesquisas referentes às Doenças Raras e Anomalias Congênitas, pesquisas essas, absolutamente imprescindíveis na atual realidade que vivemos. Diante do exposto contamos com a presença indispensável de todos vocês, Cientistas, Docentes e Discentes de graduação e pós-graduação, Profissionais da área de saúde e áreas afins além do público em geral que tenha interesse nessa área de conhecimento. ',
    headLine: '', 
    lightText: true, 
    topLine: '',
    img: require('../../images/Presidente1.png').default,
    alt: 'Image',
    start: ''
}
 
/* export const homeObjThree = {
    lightBg: false,
    primary: false,
    imgStart:'',
    lightTopLine: true, 
    lightTextDesc: true,
    buttonLabel: 'Get Started', 
    address: '',
    description: 'We help business owners increase their revenue. Our team of unique specialist can help you achieve business goals',
    headLine: 'Lead Generation Specialist for Online Businesses', 
    lightText: true, 
    topLine: 'Marketing Agency',
    img: require('../../images/svg-2.svg').default,
    alt: 'Image',
    start: ''
}  */

/* export const homeObjFour = {
    lightBg: false,
    primary: false,
    imgStart:'start',
    lightTopLine: true, 
    lightTextDesc: true,
    buttonLabel: 'Get Started', 
    address: '',
    description: 'We help business owners increase their revenue. Our team of unique specialist can help you achieve business goals',
    headLine: 'Lead Generation Specialist for Online Businesses', 
    lightText: true, 
    topLine: 'Marketing Agency',
    img: require('../../images/svg-3.svg').default,
    alt: 'Image',
    start: 'true'
} */