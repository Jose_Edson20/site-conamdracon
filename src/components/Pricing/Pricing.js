import React from 'react';
import { Button } from '../../globalStyles';
import { GiCrystalBars } from 'react-icons/gi';
import { GiCutDiamond /* GiRock */ } from 'react-icons/gi';
import { IconContext } from 'react-icons/lib';
import {
  PricingSection,
  PricingWrapper,
  PricingHeading,
  PricingContainer,
  PricingCard,
  PricingCardInfo,
  PricingCardIcon,
  PricingCardPlan,
  PricingCardCost,
  PricingCardLength,
  PricingCardFeatures,
  PricingCardFeature,
  Img1,
  Img2,
  Img3
} from './Pricing.elements';

/* import patrocinador1 from '../../images/patrocinador1.png';
import patrocinador2 from '../../images/patrocinador2.png';
import patrocinador3 from '../../images/patrocinador3.png';
import patrocinador4 from '../../images/patrocinador4.png';
import Apae from '../../images/Apae.jpeg';
import TVCorreio from '../../images/TvCorreio.jpeg';
import Patria from '../../images/Patria.jpeg';
import Senac from '../../images/Senac.jpeg';
import UniespPB from '../../images/UniespPB.jpeg'; */
import Realizacao from '../../images/realizacao_.png'; 
import Parceiros from '../../images/patrocinadores_21.png'; 


function Pricing() {
  return (
    <IconContext.Provider value={{ color: '#a9b3c1', size: 64 }}>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.2714730755174!2d-34.85062328522607!3d-7.094499194876497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7acdd0e422da873%3A0x252e780cac7ff41c!2sUNIESP%20Centro%20Universit%C3%A1rio!5e0!3m2!1spt-BR!2sbr!4v1619228304960!5m2!1spt-BR!2sbr" width="100%" height="350"></iframe>
      <PricingSection>
        <PricingWrapper>
         {/*  <PricingHeading>Realização</PricingHeading> */}
          <PricingContainer>
            {/* < PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img1 src={patrocinador1} />
              
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>  */}

            {/* <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img2 src={patrocinador2} />
                   <GiRock /> 
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
  */}

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img2 src={Realizacao} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

           {/*  <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={patrocinador3} />
      
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}
          </PricingContainer> 
    
     <br/>
     <br/>

       {/*  <PricingHeading>Parceiros</PricingHeading> */}
          <PricingContainer>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img1 src={Parceiros} />
                </PricingCardIcon>
               </PricingCardInfo>
              </PricingCard>


          {/*   <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={Apae} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}


           {/*  <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={TVCorreio} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}

{/* 
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={Patria} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
 */}

            {/* <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={Senac} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}
 
{/* 
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={UniespPB} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}

{/* 
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={P} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}

               {/* </PricingCardIcon> 
               </PricingCardInfo>
            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
              <Img2 src={patrocinador2} />   */}
                  {/* <GiRock /> */}
           {/*      </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={patrocinador3} /> */}
                  {/* <GiRock /> */}
              {/*   </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> */}
            
          </PricingContainer>
        </PricingWrapper>
      </PricingSection>
    </IconContext.Provider>
  );
}
export default Pricing;