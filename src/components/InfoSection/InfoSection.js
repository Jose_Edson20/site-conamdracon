import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Button } from '../../globalStyles'
import { NavBtnLink, NavItem } from '../Navbar/Navbar.elements'

import { InfoSec, 
         InfoRow, 
         InfoColumn, 
         TextWrapper,
         TopLine,
         Heading,
         Subtitle,
         Img,
         Address,
         ImgWrapper,
        } from './InfoSection.elements';

const InfoSection = ({ 
    primary,
    lightBg, 
    imgStart, 
    lightTopLine, 
    lightTextDesc,
  /*   buttonLabel,  */
    description,
    address, 
    headLine, 
    lightText, 
    topLine,
    img,
    alt,
    start }) => {
    return (
        <>
           <InfoSec lightBg={lightBg}>
               <Container>
                    <InfoRow imgStart={imgStart}>
                        <InfoColumn>
                            <TextWrapper>
                               <TopLine lightTopLine={lightTopLine}>{topLine}</TopLine>
                                <Heading lightText={lightText}>{headLine}</Heading>
                                <Subtitle lightTextDesc={lightTextDesc}>{description}</Subtitle>
                                <Address lightTextDesc={lightTextDesc}>{address}</Address> 
                                <Link to='/sign-up'>
                                </Link>                         
                            </TextWrapper>
                       </InfoColumn>
                        <InfoColumn>
                           <ImgWrapper start={start}>
                                <Img src= {img} alt= {alt} /> 
                           </ImgWrapper>
                       </InfoColumn> 
                    </InfoRow> 
               </Container>
           </InfoSec> 
        </>
    )
}

export default InfoSection
