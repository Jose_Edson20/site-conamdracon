import React, { useState, useEffect } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { IconContext } from 'react-icons/lib';
import { Button } from '../../globalStyles';
import { 
    Nav, 
    NavbarContainer, 
    NavLogo, 
    NavIcon, 
    MobileIcon,
    NavMenu,
    NavItem,
    NavLinks,
    NavItemBtn,
    NavBtnLink
     } from './Navbar.elements';

import Img from '../../images/CONAMDRACON.png';

const Navbar = () => {
    const [click, setCLick] = useState(false)
    const [button, setButton] = useState(true);

    const handleClick = () => setCLick(!click);
    const closeMobileMenu = () => setCLick(false);

    const showButton = () => {
        if(window.innerWidth <= 960) {
            setButton(false)
        } else {
            setButton(true)
        }
    }

    useEffect(() => {
        showButton()
    }, [])

    window.addEventListener('resize', showButton);



    return (
    <>
        <IconContext.Provider value={{ color:'#fff'}}>
            <Nav>
                <NavbarContainer>
                     {/* <NavLogo to="/" onClick={closeMobileMenu}>
                        <NavIcon></NavIcon>  */}
            
                     <NavLogo to = "/">
                             <img src = {Img} alt ="Logo" height = "50px"/> 
                    </NavLogo> 
                      <MobileIcon onClick={handleClick}>
                        {click ? <FaTimes /> : <FaBars />}
                    </MobileIcon > 
                    <NavMenu onClick={handleClick} click={click}>
                        <NavItem>
                            <NavLinks to = '/'>Início</NavLinks>
                        </NavItem>
                    
                        {/* <NavItem>
                            <NavLinks to='/services'>Submissões</NavLinks>
                        </NavItem> */}
                    
                        <NavItem>
                            <NavLinks to='/Palestrantes'>Palestrantes</NavLinks>
                        </NavItem>

                        <NavItem>
                            <NavLinks to='products'>Submissões</NavLinks>
                        </NavItem>



                        <NavItem>
                            <NavBtnLink href = {'https://drive.google.com/file/d/1sMq-KRaBM-VrpTFbMCByJaaEOvq_jVWu/view'} target = '_blank'>
                                Regulamento
                           </NavBtnLink>
                        </NavItem>

                        <NavItem>
                            <NavBtnLink href = {'https://drive.google.com/file/d/1wxdhfhV3ZwLn3uuxmzWMgkMEGv-2GcwA/view?usp=sharing'} target = '_blank'>
                                Edital
                           </NavBtnLink>
                        </NavItem>

 {/*  <Button>Regulamento</Button> */} 
  {/*  <NavLinks href ={'https://drive.google.com/file/d/1sMq-KRaBM-VrpTFbMCByJaaEOvq_jVWu/view'} target = '_blank'>Regulamento</NavLinks> */}
                                          
                        <NavItemBtn>
                            {button ? (
                                <NavBtnLink href = {'https://www.sympla.com.br/ii-conamdracon---congresso-nacional-multidisciplinar-de-doencas-raras-e-anamolias-congenitas__1197965'} target = '_blank'>
                                    <Button primary>Inscrições</Button>
                                </NavBtnLink> 
                            ) : (
                                <NavBtnLink href={'https://www.sympla.com.br/ii-conamdracon---congresso-nacional-multidisciplinar-de-doencas-raras-e-anamolias-congenitas__1197965'} target = "_blank">
                                    <Button fontBig primary>
                                        Inscrições
                                    </Button>                             
                                </NavBtnLink>

                            )}
                        </NavItemBtn>
                    </NavMenu>
                </NavbarContainer>
            </Nav>
        </IconContext.Provider> 
    </>
    );
};

export default Navbar
