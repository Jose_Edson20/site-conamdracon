import React from 'react';
import { FaFacebook, FaInstagram, FaYoutube, FaTwitter, FaLinkedin }  from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { Button } from '../../globalStyles';
import { FooterContainer, 
         FooterSubscription,
         FooterSubHeading,
         FooterSubText,
         Form,
         FormInput,
         FooterLinksContainer,
         FooterLinksWrapper,
         FooterLinkItems,
         FooterLinkTitle,
         FooterLink,
         SocialMediaWrap,
         SocialLogo,
         SocialIcons,
         SocialIcon,
         SocialIconLink,
         WebsiteRights,
         WebsiteRightsLink
          } from './Footer.elements';

const Footer = () => {
    return (
        <FooterContainer>
            {/* <FooterSubscription>
                <FooterSubHeading>
                    Join our exclusive membership
                    to receive the latest news and 
                    trends
                </FooterSubHeading>
                <FooterSubText>You can unsubscribe at any time.</FooterSubText>
                <Form>
                    <FormInput name='email' type='email' placeholder='Your Email' />
                    <Button fontBig>Subscribe</Button>
                </Form>
            </FooterSubscription> */}
            <FooterLinksContainer>
                 <FooterLinksWrapper>
                  {/*  <FooterLinkItems>
                        <FooterLinkTitle>About Us</FooterLinkTitle>
                        <FooterLink to='/sign-up'>How it works</FooterLink>
                        <FooterLink to='/'>Testimonials</FooterLink>
                        <FooterLink to='/'>Careers</FooterLink>
                        <FooterLink to='/'>Investors</FooterLink>
                        <FooterLink to='/'>Terms of Service</FooterLink>
                    </FooterLinkItems>
                    <FooterLinkItems>
                        <FooterLinkTitle>Contact Us</FooterLinkTitle>
                        <FooterLink to='/sign-up'>How it works</FooterLink>
                        <FooterLink to='/'>Testimonials</FooterLink>
                        <FooterLink to='/'>Careers</FooterLink>
                        <FooterLink to='/'>Investors</FooterLink>
                        <FooterLink to='/'>Terms of Service</FooterLink>
                    </FooterLinkItems>
                </FooterLinksWrapper>
                <FooterLinksWrapper>  */}           
                    {/* <FooterLinkItems>
                        <FooterLinkTitle>Videos</FooterLinkTitle>
                        <FooterLink to='/sign-up'>How it works</FooterLink>
                        <FooterLink to='/'>Testimonials</FooterLink>
                        <FooterLink to='/'>Careers</FooterLink>
                        <FooterLink to='/'>Investors</FooterLink>
                        <FooterLink to='/'>Terms of Service</FooterLink>
                    </FooterLinkItems> */}
               {/*      <FooterLinkItems>
                        <FooterLinkTitle>Social Media</FooterLinkTitle>
                        <FooterLink to='/sign-up'>How it works</FooterLink>
                        <FooterLink to='/'>Testimonials</FooterLink>
                        <FooterLink to='/'>Careers</FooterLink>
                        <FooterLink to='/'>Investors</FooterLink>
                        <FooterLink to='/'>Terms of Service</FooterLink>
                    </FooterLinkItems> */}
                </FooterLinksWrapper>
            </FooterLinksContainer> 
            <SocialMediaWrap>
                {/* <SocialLogo to="/">
                    <SocialIcon/>
                    CONAMDRACON
                </SocialLogo> */}

               {/*  <WebsiteRights href = {''}>
                   Todos os direitos {'\u00A9'}reservados 2021
                </WebsiteRights> */}
        

           <FooterLink to='/'>
               Todos os direitos {'\u00A9'}reservados 2021
           </FooterLink>

                <SocialIcons>
                    <SocialIconLink href='https://www.facebook.com/Conamdracon' target="_blank" aria-label="Facebook">
                        <FaFacebook />
                    </SocialIconLink>
                    <SocialIconLink href='https://www.instagram.com/conamdracon/' target="_blank" aria-label="Instagram">
                        <FaInstagram />
                    </SocialIconLink>
                    <WebsiteRights>
                          Desenvolvido por
                        <WebsiteRightsLink href = "https://www.linkedin.com/in/josé-edson-1206a6187/" target = "_blank">  José Edson</WebsiteRightsLink>  
                                    &
                        <WebsiteRightsLink href = "https://www.linkedin.com/in/jobson-ribeiro-b0a77b148/" target = "_blank"> Jobson</WebsiteRightsLink>
                    </WebsiteRights>
 
                


                    {/* <SocialIconLink href={'www.youtube.com/watch?v=iP_HqoCuRI0&t=4361s'} target="_blank" aria-label="YouTube" rel="noopener">
                        <FaYoutube />
                    </SocialIconLink> */}
                    {/* <SocialIconLink href='/' target="_blank" aria-label="Twitter">
                        <FaTwitter />
                    </SocialIconLink> */}
                   {/*  <SocialIconLink href='/' target="_blank" aria-label="LinkedIn">
                        <FaLinkedin />
                    </SocialIconLink> */}
                </SocialIcons>

            </SocialMediaWrap>        
        </FooterContainer>
    )
}

export default Footer
