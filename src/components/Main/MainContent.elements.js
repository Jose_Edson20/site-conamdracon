import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const PricingSection = styled.div`
  padding: 100px 0 50px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: #fff;
`;

export const PricingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  @media screen and (max-width: 960px) {
    margin: 0 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const PricingHeading = styled.h2`
  color: #005881;
  font-size: 48px;
  margin-bottom: 54px;
  margin-top: -15px;
`;

export const PricingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 960px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
  }
`;

export const TextWrapper = styled.div`
    max-width: 80%;
    padding-top: 0;
    padding-bottom: 60px;

    @media screen and (max-width: 768px) {
        padding-bottom: 65px;
    }
`;

export const TopLine = styled.div`
    color: #000;
    font-size: 18px;
    line-height: 25px;
    font-weight: 700px;
    letter-spacing: 1.7px;
    margin-bottom: 16px;
    text-align: justify;
    text-justify: inter-word;
`;

export const TopLineTwo = styled.p`
    color: #000;
    font-size: 14px;
    line-height: 25px;
    font-weight: 550;
    letter-spacing: 1.7px;
    margin-bottom: 16px;
    text-align: justify;
    font-family: sans-serif;
    text-justify: inter-word;
`;
export const TopLineThree = styled.p`
    color: #000;
    font-size: 14px;
    line-height: 25px;
    font-weight: 560;
    font-family: arial;
    letter-spacing: 1.7px;
    margin-bottom: 16px;
    text-align: justify;
    text-justify: inter-word;
`;
 
export const TopLineLink = styled.a`
    color: #000;
    font-size: 14px;
    font-weight: bold;
    text-decoration: none;
`;

export const Heading = styled.h1`
    margin-bottom: 24px;
    font-size: 48px;
    line-height: 1.1;
    color: #f7f8fa; 
`;

export const Subtitle = styled.p`
    max-width: 440px;
    margin-bottom: 35px;
    font-size: 18px;
    line-height: 24px;
    color: #000;
`;

