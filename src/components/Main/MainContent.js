import React from 'react';
import { Button } from '../../globalStyles';
import { GiCrystalBars } from 'react-icons/gi';
import { GiCutDiamond /* GiRock */ } from 'react-icons/gi';
import { IconContext } from 'react-icons/lib';
import {
  PricingSection,
  PricingWrapper,
  PricingHeading,
  PricingContainer,
  TextWrapper,
  TopLine,
  TopLineTwo,
  TopLineThree,
  TopLineLink,
  Heading,
  Subtitle
} from './MainContent.elements'




function MainContent() {
  return (
    <IconContext.Provider value={{ color: '#a9b3c1', size: 64 }}>
      <PricingSection>
        <PricingWrapper>
          <PricingHeading>BEM-VINDO(A) A 2ª EDIÇÃO DO CONAMDRACON</PricingHeading>
          <PricingContainer>
          <TextWrapper>
            <TopLine> 
                 João Pessoa, capital da Paraíba, mais uma vez sediará o Maior Evento sobre Doenças Raras do Brasil. Entre os dias 03 a 05 de novembro de 2021 estaremos reunidos e convidamos você a fazer parte do II Congresso Nacional Multidisciplinar sobre Doenças Raras e Anomalias Congênitas – CONAMDRACON – 
            </TopLine>
            <TopLine>
                 Estarão presentes os maiores Especialistas do País discutindo sobre uma temática que abrangerá todos os avanços e dificuldades da atualidade envolvendo as Doenças Raras nos mais variados segmentos.
                 As atividades do CONAMDRACON - 2021 durarão 03 (três) dias e consistirão de palestras e mesas redondas multiprofissionais além de apresentações orais e de pôsteres.
            </TopLine>
            <TopLine>
                Aguardamos toda a Sociedade pois as causas relacionadas aos pacientes portadores de Doenças Raras tornaram-se uma questão social e de Saúde Pública de tão grande relevância que não há mais a menor possibilidade de ser ignorada.
            </TopLine>
            <TopLine>
                Nós, enquanto cidadãos, temos muito a contribuir e aprender. Nesse Evento acontecerão inúmeras oportunidades de compartilhar conhecimentos e enriquecer ainda mais nossas experiências, trazendo várias novas perspectivas para as nossas vidas. 
            </TopLine>
            <TopLine>
                Estamos ansiosos para vê-lo em João Pessoa-PB.
            </TopLine>
              <br/>
          
                <TopLineTwo>
                         Saionara Ferreira de Araújo - Presidente do II CONAMDRACON
                  </TopLineTwo>
                         
                  <TopLineThree>
                        Email: saionararahulw@gmail.com - conamdracon@gmail.com
                  </TopLineThree>
            
            
          </TextWrapper>                      
          </PricingContainer>
        </PricingWrapper>
      </PricingSection>
    </IconContext.Provider>
  );
}
export default MainContent;