export { default as Navbar } from './Navbar/Navbar';
export { default as Footer } from './Footer/Footer';
export { default as Pricing } from './Pricing/Pricing';
export { default as InfoSection } from './InfoSection/InfoSection';
export { default as MainSection } from './Main/MainContent';
export { default as map } from './map/map';
export { default as Carousel } from './Carousel/CarouselContainer';
export { default as Palestrantes } from './Palestrantes/Palestrantes';




