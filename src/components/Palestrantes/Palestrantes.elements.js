import { Link } from 'react-router-dom';
import styled from 'styled-components';


export const PricingSection = styled.div`
  padding: 50px 0 0;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection2 = styled.div`
  padding: 50px 0 0;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection3 = styled.div`
  padding: 50px 0 0;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection4 = styled.div`
  padding: 50px 0 80px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;
export const PricingSection5 = styled.div`
  padding: 0 0 100px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;
export const PricingSection6 = styled.div`
  padding: 0 0 100px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;
export const PricingSection7 = styled.div`
  padding: 0 0 60px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;
export const PricingSection8 = styled.div`
  padding: 0 0 60px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection9 = styled.div`
  padding: 0 0 60px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection10 = styled.div`
  padding: 0 0 60px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection11 = styled.div`
  padding: 0 0 60px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;

export const PricingSection12 = styled.div`
  padding: 0 0 60px;
  display: flex;
  flex-direction: wrap;
  justify-content: center;
  background: #FFF;
`;


export const PricingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  @media screen and (max-width: 960px) {
    margin: 0px 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const TextoTitle = styled.p`
     color: #000;
     margin: 0 auto;
     padding-bottom: 20px;
`;

export const TextoLink = styled.a`
     color: #000;
     cursor: pointer;
`;
 

export const PricingHeading = styled.h1`
  color: #005881;
  font-size: 48px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const PricingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 960px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 40px;
  }
`;

export const PricingCard = styled.a`
  background: #fff;
  box-shadow: 0 6px 20px rgba(56, 125, 255, 0.8);
  width: 280px;
  height: 420px;
  text-decoration: none;
  border-radius: 4px;
 /*  &:nth-child(0)   */{
    margin: 24px;
  }
  &:hover {
    transform: scale(1.06);
    transition: all 0.3s ease-out;
    color: #fff;
  }
  @media screen and (max-width: 960px) {
    width: 90%;
    &:hover {
      transform: none;
    }
  }
}
`;

export const PricingCardInfo = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 100px;
  padding: 24px;
  align-items: center;
  color: #fff;
`;

export const PricingCardIcon = styled.div`
  margin: 50px 0;
  vertical-align: middle;
`;

export const PricingCardPlan = styled.h3`
  margin-bottom: 5px;
  font-size: 24px;
`;

export const PricingCardCost = styled.h4`
  font-size: 40px;
`;

export const PricingCardLength = styled.p`
  font-size: 14px;
  margin-bottom: 24px;
`;

export const PricingCardFeatures = styled.ul`
  margin: 16px 0 32px;
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  color: #a9b3c1;
`;

export const PricingCardFeature = styled.li`
  margin-bottom: 10px;
`;

export const Img1 = styled.img`
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;

export const Img2 = styled.img`
    margin-top: 4px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img3 = styled.img`
    margin-top: 4px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img4 = styled.img`
    margin-top: 4px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img5 = styled.img`
    margin-top: -10px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img6 = styled.img`
    margin-top: -2px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img7 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img8 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img9 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img10 = styled.img`
    margin-top: 7px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img11 = styled.img`
    margin-top: 12px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img12 = styled.img`
    margin-top: 12px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img13 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img14 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img15 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img16 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img17 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img18 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img19 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img20 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img21 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img22 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img23 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img24 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img25 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img26 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img27 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img28 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
export const Img29 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img30 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img31 = styled.img`
    margin-top: 2px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img32 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img33 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img34 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img35 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img36 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img37 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img38 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img39 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img40 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img41 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img42 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img43 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
 
export const Img44 = styled.img`
    margin-top: 2px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
 
export const Img45 = styled.img`
    margin-top: 2px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img46 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 
export const Img47 = styled.img`
    margin-top: 22px;
    padding-right: 0;
    border: 0;
    max-width: 100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 250px;
`;
 