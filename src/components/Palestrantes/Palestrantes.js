import React from 'react';
import { Button } from '../../globalStyles';
import { IconContext } from 'react-icons/lib';
import {TextoTitle, TextoLink} from '../Palestrantes/Palestrantes.elements'
import {NavBtnLink, } from '../Navbar/Navbar.elements';
 
import {
    PricingSection,
    PricingSection2,
    PricingSection3,
    PricingSection4,
    PricingSection5,
    PricingSection6,
    PricingSection7,
    PricingSection8,
    PricingSection9,
    PricingSection10,
    PricingSection11,
    PricingSection12,
    PricingWrapper,
    PricingHeading,
    PricingContainer,
    PricingCard,
    PricingCardInfo,
    PricingCardIcon,
    PricingCardPlan,
    PricingCardCost,
    PricingCardLength,
    PricingCardFeatures,
    PricingCardFeature,
    Img1,
    Img2,
    Img3,
    Img4,
    Img5,
    Img6,
    Img7,
    Img8,
    Img9,
    Img10,
    Img11,
    Img12,
    Img13,
    Img14,
    Img15,
    Img16,
    Img17,
    Img18,
    Img19,
    Img20,
    Img21,
    Img22,
    Img23,
    Img24,
    Img25,
    Img26,
    Img27,
    Img28,
    Img29,
    Img30,
    Img31,
    Img32,
    Img33,
    Img34,
    Img35,
    Img36,
    Img37,
    Img38,
    Img39,
    Img40,
    Img41,
    Img42,
    Img43,
    Img44,
    Img45,
    Img46,
    Img47
  } from '../Palestrantes/Palestrantes.elements';

import Image1 from '../../images/Priscila.jpg';
import Image2 from '../../images/Adriana.jpg';
import Image3 from '../../images/Dra.Eutilia.jpg';
import Image4 from '../../images/Arnaldo.jpg';
import Image5 from '../../images/Roberto.jpg';
import Image6 from '../../images/AnaMaria.jpg';
import Image7 from '../../images/Jeanine.jpg';
import Image8 from '../../images/Patricia_Longo.jpg';
import Image9 from '../../images/Joacilda.jpg';
import Image10 from '../../images/Maria.jpg';
import Image11 from '../../images/Giovanni.jpg';
import Image12 from '../../images/Paula.jpg';
import Image13 from '../../images/Isabella.jpg';
import Image14 from '../../images/Flavia.jpg';
import Image15 from '../../images/Mayra.jpg';
import Image16 from '../../images/Saionara.jpg';
import Image17 from '../../images/Moyra.jpg';
import Image18 from '../../images/Renata.jpg';
import Image19 from '../../images/Mayara.jpg';
import Image20 from '../../images/Alberlene.jpg';
import Image21 from '../../images/Leonya.jpg';
import Image22 from '../../images/Angela.jpg';
import Image23 from '../../images/Isabel.jpg';
import Image24 from '../../images/Ana_Paula.jpg';
import Image25 from '../../images/Keyla.jpg';
import Image26 from '../../images/Francisco.jpg';
import Image27 from '../../images/Dr.Rafael_Souza.gif';
import Image28 from '../../images/Hermano.jpg';
import Image29 from '../../images/Rubens.jpg';
import Image30 from '../../images/Joao_Carlos.jpg';
import Image31 from '../../images/Antonio-Fausto.jpg';
import Image32 from '../../images/Wellington.jpg';
import Image33 from '../../images/Karine.jpg';
import Image34 from '../../images/Lucrecia.jpg';
import Image35 from '../../images/Marcelo.jpg';
import Image36 from '../../images/Ronaldo.jpg';
import Image37 from '../../images/Luan.jpg';
import Image38 from '../../images/Camila.jpg';
import Image39 from '../../images/Claudio.jpg';
import Image40 from '../../images/Juliano.jpg';
import Image41 from '../../images/Solange.jpg';
import Image42 from '../../images/Joseane.jpg';
import Image43 from '../../images/Juliana.png';
import Image44 from '../../images/Sidcleia.jpg';
import Image45 from '../../images/Katya.jpg';
import Image46 from '../../images/Mariani.jpg';
import Image47 from '../../images/Adir.jpg';


function Palestrantes(){
    return (
        <IconContext.Provider value={{ color: '#a9b3c1', size: 64 }}>
          <PricingSection>
            <PricingWrapper>
              <PricingHeading>Palestrantes</PricingHeading>
                <PricingContainer>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <TextoTitle>
                      <TextoLink href = {'http://lattes.cnpq.br/6549241028396874'} target = "_blank">
                         Priscilla Gaspar de Oliveira
                      </TextoLink>
                  </TextoTitle>
                     <Img1 src={Image1} />  
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = {'http://lattes.cnpq.br/0075445682468122'} target = "_blank" >
                <TextoTitle>
                       Adriana Ramos Silva Pinheiro
                  </TextoTitle>
                  </TextoLink> 
                    <Img2 src={Image2} />
                  
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            
                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoTitle>
                     <TextoLink href = {'http://lattes.cnpq.br/3174500275749680'} target = "_blank" >
                        Eutilia Andrade Medeiros Freire
                   </TextoLink> 
                  </TextoTitle>
                  <Img3 src={Image3} />    
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

             <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <TextoTitle>
                      <TextoLink href = "http://lattes.cnpq.br/8023869177216297" target = "_blank">
                           Roberto Giugliani
                      </TextoLink>
                  </TextoTitle>
                     <Img5 src={Image5} />   
                </PricingCardIcon>
              </PricingCardInfo>
             </PricingCard> 
           </PricingContainer>
         </PricingWrapper>
        </PricingSection>

          <PricingSection2>
            <PricingWrapper>
                <PricingContainer>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/0570864724558353" target = "_blank">  
                     Maria Enedina Aquino Scuarcialupi
                 </TextoLink>
                  <Img10 src={Image10} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            
               <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <TextoTitle>
                      <TextoLink href = "http://lattes.cnpq.br/9599036707326053" target = "_blank">
                            Ana Maria Ribeiro de Moura
                      </TextoLink>
                  </TextoTitle>
                     <Img6 src={Image6} />   
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>  

              <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/3963088995523705" target = "_blank">  
                   Jeanine Aparecida Magno Frantz   
                 </TextoLink>
                  <Img7 src={Image7} />
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard> 

                <PricingCard>
              <PricingCardInfo>
               <PricingCardIcon>
               <TextoLink  href = {'http://lattes.cnpq.br/4266138467935039'} target = '_blank'> 
                      Patricia Longo Ribeiro Delai    
                </TextoLink>      
                  <Img8 src={Image8} /> 
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


            </PricingContainer>
            </PricingWrapper>
          </PricingSection2>

          <PricingSection3>
            <PricingWrapper>
              <PricingContainer>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/781061582942825" target = "_blank">  
                    Joacilda da Conceicao Nunes
                 </TextoLink>
                  <Img9 src={Image9} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/1315306968454418" target = "_blank">  
                    Flávia Cristina Fernandes Pimenta
                 </TextoLink>
                  <Img14 src={Image14} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            
                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/3608234302688315" target = "_blank">  
                     Constantino Giovanni Braga Cartaxo
                 </TextoLink>
                  <Img11 src={Image11} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/5467776934365558" target = "_blank">  
                    Paula Frassinetti Vasconcelos de Medeiros
                 </TextoLink>
                  <Img12 src={Image12} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

          </PricingContainer>
          </PricingWrapper>
          </PricingSection3>

            <PricingSection4>
              <PricingWrapper>
                <PricingContainer>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = {"http://lattes.cnpq.br/5986610995484608"} target = "_blank">  
                    Isabella Araújo Mota Fernandes
                 </TextoLink>
                  <Img13 src={Image13} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

          <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/0074743373990701" target = "_blank">  
                     Renata da Silveira Rodrigues Paiva
                 </TextoLink>
                  <Img18 src={Image18} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

               <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/6868994397359491" target = "_blank">  
                      Mayra de Freitas Montenegro
                 </TextoLink>
                  <Img15 src={Image15} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

                <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/0386066444973093" target = "_blank">  
                      Saionara Ferreira de Araújo
                 </TextoLink>
                  <Img16 src={Image16} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            
                </PricingContainer>
              </PricingWrapper>
            </PricingSection4>
              
              <PricingSection5>
                <PricingWrapper>
                  <PricingContainer>

                  <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "" target = "_blank">  
                      Moyra Ferreira Araújo de Freitas 
                 </TextoLink>
                  <Img17 src={Image17} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

                {/*   <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/0074743373990701" target = "_blank">  
                     Renata da Silveira Rodrigues Paiva
                 </TextoLink>
                  <Img18 src={Image18} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
 */}


          <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/3773221366817617" target = "_blank">  
                     Isabel Carolina da Silva Pinto 
                 </TextoLink>
                  <Img23 src={Image23} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


                  <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/4912693344994315" target = "_blank">  
                       Mayara Araújo dos Santos
                 </TextoLink>
                  <Img19 src={Image19} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


                  <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/8529583512077563" target = "_blank">  
                    Alberlene Baracho Sales
                 </TextoLink>
                  <Img20 src={Image20} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

              
              </PricingContainer>
            </PricingWrapper>
          </PricingSection5>


        <PricingSection6>
          <PricingWrapper>
            <PricingContainer>

          
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/3172251605458926" target = "_blank">  
                     Leonnya Dayse Araujo Pinheiro
                 </TextoLink>
                  <Img21 src={Image21} />        
                </PricingCardIcon>
              </PricingCardInfo>

            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/1241631355870424" target = "_blank">  
                     Ângela Amorim de Araújo
                 </TextoLink>
                  <Img22 src={Image22} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            
           {/*  <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/3773221366817617" target = "_blank">  
                     Isabel Carolina da Silva Pinto 
                 </TextoLink>
                  <Img23 src={Image23} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
 */}


            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/1944471833276023" target = "_blank">  
                       Ana Paula Morais Braga
                 </TextoLink>
                  <Img24 src={Image24} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/3086818599920651" target = "_blank">  
                      Rubens Batista Benedito
                 </TextoLink>
                  <Img29 src={Image29} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            </PricingContainer>
          </PricingWrapper>
        </PricingSection6>

        <PricingSection7>
          <PricingWrapper>
            <PricingContainer>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/8620975147127924" target = "_blank">  
                      Keila Maruze de França  
                 </TextoLink>
                  <Img25 src={Image25} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/5697758072784254" target = "_blank">  
                      Francisco de Assis Limeira Junior
                 </TextoLink>
                  <Img26 src={Image26} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/1678609255617074" target = "_blank">  
                     Rafael de Souza Andrade
                 </TextoLink>
                  <Img27 src={Image27} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/9306111209449454" target = "_blank">          
                      Hermano Jose Falcone de Almeida
                 </TextoLink>
                  <Img28 src={Image28} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


{/* 
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/9306111209449454" target = "_blank">  
                       Hermano Jose Falcone de Almeida
                 </TextoLink>
                  <Img28 src={Image28} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>  */}


            </PricingContainer>
          </PricingWrapper>
        </PricingSection7>

        <PricingSection8>
          <PricingWrapper>
            <PricingContainer>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/9197783824518804" target = "_blank">          
                       João Carlos Lima Rodrigues Pita   
                 </TextoLink>
                  <Img30 src={Image30} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
 

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "https://www.escavador.com/sobre/10214401/antonio-fausto-terceiro-de-almeida" target = "_blank">          
                     Antonio Fausto Terceiro de Almeida
                </TextoLink>
                  <Img31 src={Image31} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/5046466798833423" target = "_blank">          
                       Wellington Cavalcanti de Araújo
                </TextoLink>
                  <Img32 src={Image32} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                <TextoLink href = "http://lattes.cnpq.br/4162138065576578" target = "_blank">          
                       Karine Amado Garcia
                </TextoLink>
                  <Img33 src={Image33} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


              </PricingContainer>
            </PricingWrapper>
          </PricingSection8>

          <PricingSection9>
             <PricingWrapper>
               <PricingContainer>

               <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/7018405190423994" target = "_blank">          
                           Maria Lucrécia de Aquino Gouveia
                     </TextoLink>
                  <Img34 src={Image34} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

               <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/6758496555468600" target = "_blank">          
                          Marcelo Gonçalves Sousa
                     </TextoLink>
                  <Img35 src={Image35} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>


               <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/7360209850549532" target = "_blank">          
                            Ronaldo Rangel Travassos Júnior
                     </TextoLink>
                  <Img36 src={Image36} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

            <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/3887768837565223" target = "_blank">          
                           Lucas Victor Alves
                     </TextoLink>
                  <Img37 src={Image37} />        
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>

              </PricingContainer>
            </PricingWrapper>
          </PricingSection9>

              <PricingSection10>
                <PricingWrapper>
                  <PricingContainer>

             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/8516305432771521" target = "_blank">          
                           Camila Batista Nóbrega
                      </TextoLink>
                    <Img38 src={Image38} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>

             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/8705663284049857" target = "_blank">          
                             Claúdio Teixeira Régis
                      </TextoLink>
                    <Img39 src={Image39} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>

             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/3066755908185577" target = "_blank">          
                            Juliano Maciel Pereira
                      </TextoLink>
                    <Img40 src={Image40} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>


             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/8569871950150145" target = "_blank">          
                          Solange Fátima Geraldo da Costa
                      </TextoLink>
                    <Img41 src={Image41} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>


            </PricingContainer>
          </PricingWrapper>
        </PricingSection10>



        <PricingSection11>
          <PricingWrapper> 
             <PricingContainer>



             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/0583444909190284" target = "_blank">          
                          Joseane da Silva Leite
                      </TextoLink>
                    <Img42 src={Image42} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>


             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/3094033583031765" target = "_blank">          
                            Juliana Sousa Soares de Araújo
                      </TextoLink>
                    <Img43 src={Image43} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>

             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/5047282524063662" target = "_blank">          
                          Sidcléia Onorato Arruda Vasconcelos
                      </TextoLink>
                    <Img44 src={Image44} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>


             <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/8585902794071018" target = "_blank">          
                            Cristina Kátya Torres Teixeira Mendes
                      </TextoLink>
                    <Img45 src={Image45} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>



            </PricingContainer>
          </PricingWrapper>
        </PricingSection11>



         <PricingSection12>
           <PricingWrapper>
              <PricingContainer>



              <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/0389843286786480" target = "_blank">          
                            Mariani de Oliveira e Silva
                      </TextoLink>
                    <Img46 src={Image46} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>


              <PricingCard>
                 <PricingCardInfo>
                   <PricingCardIcon>
                     <TextoLink href = "http://lattes.cnpq.br/6568929910430411" target = "_blank">          
                           Adir Fátima da Rosa Andrade
                      </TextoLink>
                    <Img47 src={Image47} />        
                  </PricingCardIcon>
               </PricingCardInfo>
           </PricingCard>



                </PricingContainer>
            </PricingWrapper>
        </PricingSection12>
     </IconContext.Provider>
    )
}
export default Palestrantes;